FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /ayomiDjangoTestDouma
WORKDIR /ayomiDjangoTestDouma
ADD requirements.txt /ayomiDjangoTestDouma/
RUN pip install -r requirements.txt
ADD . /ayomiDjangoTestDouma/
