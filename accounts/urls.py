from django.urls import path
from . import views
from .views import ArticleListView

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('article/', views.ArticleListView.as_view(), name='article_list'),
]