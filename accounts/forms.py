from msilib.schema import ListView

from django.contrib.auth.models import User


# class UserModelForm(BSModalModelForm):
#     class Meta:
#         model = User
#         exclude = ['timestamp']

class UserListView(ListView):
    model = User
    template_name = 'template/home.html'

    def get_queryset(self):
        return User.objects.all()