
# Create your views here.
from time import timezone

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import ListView
from accounts.models import Article


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
#

class UserListView(ListView):
    model = User

    def get_queryset(self):
        return User.objects.all()


class ArticleListView(ListView):
    model = Article
    paginate_by = 100  # if pagination is desired
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context